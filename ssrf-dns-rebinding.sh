#!/bin/bash

echo $1 > _tmp/strip

# Run first web server, serving internal documents
php -S 127.0.0.1:8080 -t ./ssrf-dns-rebinding/internal/ &

# Run second web server, serving external documents
php -S 127.0.0.1:8081 -t ./ssrf-dns-rebinding/external/ &

open 'http://127.0.0.1:8081/?host=test.com'

wait
