<?php
require_once('../_helpers/strip.php');

// the `page` variable isn't getting sanitized, making it vulnerable
// to a path traversal vulnerability.
echo file_get_contents($_GET['page']);
?>
