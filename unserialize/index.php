<?php

require_once('../_helpers/strip.php');

// Define a class with the __destruct method. As can be seen,
// there is no code path that allows the user to overwrite the
// $variable variable in the Foo class. However, with a vulnerable
// unserialize call, we can.
class Foo {
  public $variable = 'Hello world';

  function __destruct() {
    echo $this->variable;
  }
}

// Pass user input directly into unserialize
unserialize($_GET['object']);
